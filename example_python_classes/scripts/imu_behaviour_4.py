#!/usr/bin/env python

import rospy
from sensor_msgs.msg import Imu
from tf.transformations import euler_from_quaternion
from gurdy_mover import gurdyJointMover
from imu_data_processor import GurdyImuDataProcessor


class GurdyBehaviour(object):

    def __init__(self):

        self.imu_data_processor = GurdyImuDataProcessor()
        self.gurdy_mover = gurdyJointMover()


    def choose_behaviour(self):
        detected_upsidedown = self.imu_data_processor.is_upasidedown()
        # Do something based on detect_upsidedown
        if detected_upsidedown:
            movement = "flip"
        else:
            movement = "basic_stance"
        rospy.logwarn(str(movement))
        self.gurdy_mover.execute_movement_gurdy(movement)

    def start_behaviour(self):

        while not rospy.is_shutdown():
            self.choose_behaviour()




def start_imu_behaviour():
    # We Start Here
    rospy.init_node('imu_bahaviour_node')
    gurdy_bhv = GurdyBehaviour()
    gurdy_bhv.start_behaviour()


if __name__ == "__main__":
    start_imu_behaviour()

