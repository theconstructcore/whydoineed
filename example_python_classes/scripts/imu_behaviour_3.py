#!/usr/bin/env python

import rospy
from sensor_msgs.msg import Imu
from tf.transformations import euler_from_quaternion
from gurdy_mover import gurdyJointMover



class GurdyBehaviour(object):

    def __init__(self):

        self.gurdy_mover = gurdyJointMover()
        self.sub = rospy.Subscriber ('/gurdy/imu/data', Imu, self.get_imu_data)

    def extract_rpy_imu_data (self, imu_msg):

        orientation_quaternion = imu_msg.orientation
        orientation_list = [orientation_quaternion.x,
                            orientation_quaternion.y,
                            orientation_quaternion.z,
                            orientation_quaternion.w]
        roll, pitch, yaw = euler_from_quaternion (orientation_list)
        return roll, pitch, yaw


    def detect_upsidedown(self, roll, pitch, yaw):

        detected_upsidedown = False

        rospy.loginfo("[roll, pitch, yaw]=["+str(roll)+","+str(pitch)+","+str(yaw)+"]")

        # UpRight: 0.011138009176,0.467822324143,2.45108157992

        # UpSideDown: [-3.1415891735,-2.12226602154e-05,2.38423951221]
        # UpSideDown: [3.14141489641,-0.000114323270767,2.38379058991]

        roll_trigger = 3.0

        if abs(roll) > roll_trigger:
            rospy.logwarn("UPASIDEDOWN-ROLL!")
            detected_upsidedown = True

        return detected_upsidedown


    def get_imu_data(self, msg):
        roll, pitch, yaw = self.extract_rpy_imu_data(msg)
        detected_upsidedown = self.detect_upsidedown(roll, pitch, yaw)
        # Do something based on detect_upsidedown
        if detected_upsidedown:
            movement = "flip"
        else:
            movement = "basic_stance"
        rospy.logwarn(str(movement))
        #self.gurdy_mover.execute_movement_gurdy(movement)



def start_imu_behaviour():
    # We Start Here
    rospy.init_node('imu_bahaviour_node')
    gurdy_bhv = GurdyBehaviour()
    rospy.spin()


if __name__ == "__main__":
    start_imu_behaviour()

